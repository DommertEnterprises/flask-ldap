Flask-LDAP
==========

Flask-LDAP is an extension to Flask that allows you to easily add LDAP based authentication to your website. It depends on Flask, python-ldap and Flask-PyMongo(optionally). 

Documentation
-------------

http://flask-ldap.readthedocs.org/
